import requests
import pandas as pd
from io import StringIO
from datetime import datetime
import time
import sqlite3
import os
import random
import json
from dateutil.rrule import rrule, DAILY, MONTHLY 

def get_interval(conn,table):
    sql = 'SELECT DISTINCT date FROM ' + str(table)
    date_series = pd.read_sql(sql, conn)

    date_first = date_series.min()
    date_last = date_series.max()

    interval_str = date_first.date + '~' + date_last.date

    print(str(table) +' interval is :', interval_str)

def get_taiex(conn):
    
    sql = 'SELECT * FROM taiex'
        
    df = pd.read_sql(sql, conn, parse_dates=['date']).sort_values(by=['date'])

    return df.set_index(['date'])

def get_info(conn):

    sql = 'SELECT * FROM info'
    df_info = pd.read_sql(sql, conn)

    return df_info.set_index('stock_id')

def get_data(conn, table):
    
    sql = 'SELECT * FROM ' + str(table)
        
    df = pd.read_sql(sql, conn, parse_dates=['date']).sort_values(by=['date'])

    return df.set_index(['stock_id','date'])

def get_data_by_date(conn, table, start_date, end_date):
    # for demo by date interval 
    start = start_date.strftime('%Y-%m-%d')
    end = end_date.strftime('%Y-%m-%d')
    
    sql = 'SELECT * FROM ' + str(table) + ' WHERE date > "' + start +'"' + ' AND date < "' + end +'"' 
        
    df = pd.read_sql(sql, conn, parse_dates=['date']).sort_values(by=['date'])

    return df.set_index(['stock_id','date'])

def get_statement_inderval(conn, sheet_name):
    # get raw data
    sql_rdata = 'SELECT * FROM {}_rdata'.format(sheet_name)
    rdata = pd.read_sql(sql_rdata, conn , index_col=['stock_id', 'date'])

    # get converted data
    sql_conv = 'SELECT * FROM {}'.format(sheet_name)
    convdata = pd.read_sql(sql_conv, conn, index_col=['stock_id', 'date'])

    rdata_interval = rdata.get(['年度','季度']).drop_duplicates().reset_index()
    conv_interval = convdata.get(['年度','季度']).drop_duplicates().reset_index()

    print('{} raw data interval :'.format(sheet_name))
    print(rdata_interval.get(['年度','季度']).drop_duplicates().reset_index().set_index('年度').get('季度').T)
    print('{} converted data interval :'.format(sheet_name))
    print(conv_interval.get(['年度','季度']).drop_duplicates().reset_index().set_index('年度').get('季度').T)
