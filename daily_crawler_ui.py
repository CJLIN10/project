# 爬取每日盤後交易資訊(收盤價/本益比/三大法人))

conn = sqlite3.connect(os.path.join('data', "stock_data.db"))

style = {'description_width': 'initial'}

btn_date_start = widgets.DatePicker(
    disabled=False,
    value = datetime.today()
)

btn_date_end = widgets.DatePicker(
    disabled=False,
    value = datetime.today()
)

btn_crawl = widgets.Button(
    description='Crawl'
)

radio_method = widgets.RadioButtons(
    options={'test': 'test', 'merge':'merge', 'replace':'replace' },
    value='test',
    description='Method:',
    disabled=False,
    style = style
)

radio_target = widgets.RadioButtons(
    options={'price': 'price', 'per': 'per', 'legalperson': 'legalperson' },
    value='price',
    description='Crawl target:',
    disabled=False,
    style = style
)

def crawl_target(b):
    
    start = btn_date_start.value
    end = btn_date_end.value
    target = radio_target.value
    method = radio_method.value
    
    date_interval = list(rrule(DAILY, dtstart=start, until=end))
    df = pd.DataFrame()
    
    print('start crawl target: ' + target +' method: ' + method)
    
    for date in date_interval :

        print(date.strftime('%Y-%m-%d'))

        try:
            if target == 'per':
                twse_per = crawl_twse_per(date)
                tpex_per = crawl_tpex_per(date)  
                df = df.append(twse_per).append(tpex_per)
                
            elif target == 'price':
                twse_price = crawl_twse_price(date)
                tpex_price = crawl_tpex_price(date)
                df = df.append(twse_price).append(tpex_price)
            
            elif target == 'legalperson':
                twse_legalperson = crawl_twse_legalperson(date)
                tpex_legalperson = crawl_tpex_legalperson(date)
                df = df.append(twse_legalperson).append(tpex_legalperson)
                
        except:
            print('no data at ' + date.strftime('%Y-%m-%d'))
            continue
        
        time.sleep(5 + random.randint(0,5))
    
    insert_table(conn, df , target , method)
    print('finish')    
    

btn_crawl.on_click(crawl_target) 
get_interval(conn,'price')
get_interval(conn,'per')
get_interval(conn,'legalperson')

items = [radio_target, radio_method, btn_date_start, btn_date_end ,btn_crawl]
widgets.VBox(items)