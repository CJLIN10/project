#coding:utf-8

import requests
import pandas as pd
from io import StringIO
from datetime import datetime
import time
import sqlite3
import os
import random
import json
from dateutil.rrule import rrule, DAILY, MONTHLY 

def crawl_twse_price(date):
# 上市公司收盤資訊
# --- step --- 
# 1. crawl data 
# 2. data preprocessing
# 3. rename column name = stock_id, name, volume, open, close, high, low 

    # DataFrame date format
    date_report = date.strftime('%Y-%m-%d')

    # url上市公司日期格式(ex. 20190101)
    date_twse = date.strftime('%Y%m%d') 
    url_twse = 'http://www.tse.com.tw/exchangeReport/MI_INDEX?response=csv&date=' + date_twse + '&type=ALLBUT0999&_=1528951505187'
    
    #fake bowser
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    
    # crawl twse data       
    try:
        r_twse = requests.post(url_twse, headers)
    except:
        print('**WARN** requests cannot get twse data at', date_report)
        return None
    
    # data preprocessing
    lines = r_twse.text.split('\n')
    newlines = []
    
    # 指數類欄位為6欄，個股數據欄位為17欄，
    # 過濾個股數據用「",」判斷符合加入newlines 中

    for line in lines:   
        if len(line.split('",')) == 17:
            newlines.append(line)
    
    s = '\n'.join(newlines)
    s = s.replace('=', '')

    if s == '':
        return None

    df = pd.read_csv(StringIO(s))
    df.rename(columns = {'證券代號':'stock_id','證券名稱':'name', '收盤價':'close', '開盤價':'open', '最高價':'high', '最低價':'low', '成交股數':'volume'}, inplace = True)

    df['volume'] = df['volume'].apply(lambda x: str(x.replace(',','')))
    col_id = df['stock_id']
    
    df = df.apply(lambda s: pd.to_numeric(s, errors='coerce')).dropna(how='all', axis=1)
    df = df[~df['open'].isnull()]
    df = df[~df['close'].isnull()]

    df['stock_id'] = col_id
    df['date'] = date_report
    
    df = df.filter(['stock_id','date','close','volume','open','high','low']) 
    df.insert(0,'group','twse')
    
    return df.set_index(['stock_id', 'date'])

def crawl_tpex_price(date):
# 上櫃公司收盤資訊

    # DataFrame date format
    date_report = date.strftime('%Y-%m-%d')

    # url上櫃公司日期格式(ex. 107/01/01)
    year_str = str(date.year-1911)
    md_str = date.strftime('/%m/%d')
    date_tpex = year_str + md_str

    url_tpex = 'http://www.tpex.org.tw/web/stock/aftertrading/daily_close_quotes/stk_quote_result.php?l=zh-tw&d=' + date_tpex + '&_=1531718901645'

    #fake bowser
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    try:
        r_tpex = requests.post(url_tpex, headers)
        r_tpex.encoding = 'utf-8'
         
    except:
        print('**WARN** requests cannot get tpex data at', date_report)
        return None

    #data preprocessing
    jdata = json.loads(r_tpex.text)
    df = pd.DataFrame()
    
    column_name = ["stock_id","name","close","spread","open","high","low","avg","volume","成交金額","成交筆數","最後買價","最後賣價","發行股數","次日參考價","次日漲停價","次日跌停價"]     
    
    df = pd.DataFrame(jdata['aaData'], columns=list(column_name))

    if len(df) == 0:
        return None

    df['volume'] = df['volume'].apply(lambda x: str(x.replace(',','')))
    col_id = df['stock_id']
    
    df = df.apply(lambda s: pd.to_numeric(s, errors='coerce')).dropna(how='all', axis=1)
    df = df[~df['open'].isnull()]
    df = df[~df['close'].isnull()]

    df['stock_id'] = col_id
    df['date'] = date_report
    
    df = df.filter(['stock_id','date','close','volume','open','high','low'])
    df.insert(0,'group','tpex')

    return df.set_index(['stock_id', 'date'])

def crawl_twse_revenue(date):
# 上市公司月營收
    
    # 月營收日期格式轉換
    # 次月10號前公布前一個月營收，2019年1月抓取2018年報表格式為 ex. 2018-12-01

    if date.month == 1 :
        date_url = str(date.year-1912) + '_12_0'
        date_report = str(date.year - 1) + '-12-01'

    else:
        mon_str = str(date.month - 1)
        date_url = str(date.year-1911) + '_' + mon_str + '_0'

        if date.month < 11:
            date_report = str(date.year) + '-0' + mon_str +'-01'
        else:
            date_report = str(date.year) + '-' + mon_str +'-01'
    
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    url = 'https://mops.twse.com.tw/nas/t21/sii/t21sc03_'+ date_url +'.html'

    try:
        rdata = requests.get(url, headers)
        rdata.encoding = 'big5'
    except:
        print('**WARN** requests cannot get twse data at', date_report)

    dfs = pd.read_html(StringIO(rdata.text))

    df = dfs[0]
    df = df.iloc[:,:10]

    new_columns = df[df[0] == '公司代號'].iloc[0]
    df.columns = new_columns

    df['公司代號'] = pd.to_numeric(df['公司代號'], errors='coerce')
    
    df = df.dropna(subset=['公司代號'])
    
    stock_id =  df['公司代號'].astype(int).astype(str)

    name = df['公司名稱']

    df = df.apply(lambda s: pd.to_numeric(s, errors='coerce')).dropna(how='all', axis=1)
    
    df['stock_id'] = stock_id
    df['name'] = name
    df['date']= date_report
    
    df.drop('公司代號', axis=1,  inplace=True)
    df.insert(0,'group','twse')
    return df.set_index(['stock_id', 'date'])
    
def crawl_tpex_revenue(date):
# 上櫃公司月營收

    # 月營收日期格式轉換
    
    if date.month == 1 :
        date_url = str(date.year-1912) + '_12_0'
        date_report = str(date.year - 1) + '-12-01'

    else:
        mon_str = str(date.month - 1)
        date_url = str(date.year-1911) + '_' + mon_str + '_0'

        if date.month < 11:
            date_report = str(date.year) + '-0' + mon_str +'-01'
        else:
            date_report = str(date.year) + '-' + mon_str +'-01'

    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    url = 'https://mops.twse.com.tw/nas/t21/otc/t21sc03_'+ date_url +'.html'

    try:
        rdata = requests.get(url, headers)
        rdata.encoding = 'big5'
    except:
        print('**WARN** requests cannot get tpex data at', date_report)
    
    dfs = pd.read_html(StringIO(rdata.text))

    df = dfs[0]
    df = df.iloc[:,:10]

    new_columns = df[df[0] == '公司代號'].iloc[0]
    df.columns = new_columns

    df['公司代號'] = pd.to_numeric(df['公司代號'], errors='coerce')
    df = df.dropna(subset=['公司代號'])
    
    stock_id = df['公司代號'].astype(int).astype(str)
    name = df['公司名稱']

    df = df.apply(lambda s: pd.to_numeric(s, errors='coerce')).dropna(how='all', axis=1)

    df['stock_id'] = stock_id
    df['name'] = name
    df['date'] = date_report
    
    df.drop('公司代號', axis=1,  inplace=True)
    df.loc[:,'group'] = 'tpex'

    return df.set_index(['stock_id', 'date'])

def crawl_taiex(date):
    # 大盤指數

    date_url = date.strftime('%Y%m') + '01'
    
    url_taiex = 'http://www.twse.com.tw/indicesReport/MI_5MINS_HIST?response=json&date=' + date_url + '&_=1558928673887'
    
    #fake bowser
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    try:
        rdata = requests.post(url_taiex, headers)

    except:
        print('**WARN** requests cannot get taiex data')
        
    
    jdata = json.loads(rdata.text)
    column_name = ["date","open","high","low","close"]     
    
    df = pd.DataFrame(jdata['data'], columns=list(column_name))
    
    date_convert = df['date'].apply(lambda x: str(int(str(x.replace('/',''))) + 19110000))

    df['open'] = df['open'].apply(lambda x: str(x.replace(',','')))
    df['high'] = df['high'].apply(lambda x: str(x.replace(',','')))
    df['low'] = df['low'].apply(lambda x: str(x.replace(',','')))
    df['close'] = df['close'].apply(lambda x: str(x.replace(',','')))

    df = df.apply(lambda s: pd.to_numeric(s, errors='coerce')).dropna(how='all', axis=1)
    df['date'] = date_convert.apply(lambda x: x[0:4]+'-'+ x[4:6]+ '-' + x[6:8])

    return df.set_index(['date'])

def crawl_info(conn, group):
    
    if group == 'twse':
        res = requests.get('http://dts.twse.com.tw/opendata/t187ap03_L.csv')
    elif group == 'tpex':
        res = requests.get('http://mopsfin.twse.com.tw/opendata/t187ap03_O.csv')
        
    res.encoding='utf-8'
    df = pd.read_csv(StringIO(res.text))
    df['stock_id'] = df['公司代號'].astype(str)
    df = df.set_index('stock_id')
    df.insert(0,'group',group)
    return df

def crawl_twse_per(date):

    # 上市公司本益比資訊依日期查詢

    date_str = date.strftime('%Y-%m-%d')
    date_twse = date.strftime('%Y%m%d')
    
    url = 'http://www.tse.com.tw/exchangeReport/BWIBBU_d?response=csv&date='+ date_twse +'&selectType=ALL&_=1542076423875'
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    
    try:
        r = requests.post(url, headers)
    except:
        print('**WARN** requests cannot get twse data at', date_str)
        return None
    
    lines = r.text.split('\n')
    newlines = []
    
    if len(lines) <= 2:
        print('* no twse data at', date_str)
        return None
    
    else:
        for line in lines:   
            if len(line.split('",')) >= 6:
                newlines.append(line)
                
    new_group = []

    for item in newlines:   
        if len(item.split('",')) >= 6:
            item = item.replace(',\r' ,'')
            new_group.append(item)

    content = '\n'.join(new_group)

    df = pd.read_csv(StringIO(content))
    
    df['stock_id'] = df['證券代號'].astype(str)
    df['PER'] = df['本益比'].apply(lambda s:pd.to_numeric(s, errors='coerce'))
    df['yield'] = df['殖利率(%)'].apply(lambda s:pd.to_numeric(s, errors='coerce'))
    df['PBR'] = df['股價淨值比'].apply(lambda s:pd.to_numeric(s, errors='coerce'))
    
    df = df.drop(['證券代號'],axis=1)
    df = df.fillna(0)
    
    df['date'] = date_str
    df = df.filter(['stock_id','date','PER','yield','PBR'])
    df.insert(0,'group','twse')
    
    return df.set_index(['stock_id', 'date'])

def crawl_tpex_per(date):
# 上櫃公司本益比資訊依日期查詢

    date_str = date.strftime('%Y-%m-%d')
    year_str = str(date.year-1911)
    
    if (date.month < 10):
        month_str = '0' + str(date.month)
    else:
        month_str = str(date.month)
        
    if (date.day < 10):
        day_str = '0' + str(date.day)
    else:
        day_str = str(date.day)

    date_tpex = year_str +'/'+ month_str +'/'+ day_str

    url = 'http://www.tpex.org.tw/web/stock/aftertrading/peratio_analysis/pera_result.php?l=zh-tw&d='+ date_tpex +'&c=&_=1542009445182'
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    
    try: 
        r = requests.post(url, headers)
    except:
        print('**WARN** requests cannot get tpex data at', date_str)
        return None
    
    jdata = json.loads(r.text)
    
    df = pd.DataFrame()
    
    column_name = ["stock_id","name","PER","dividend","dividend_year","yield","PBR"]     

    df = pd.DataFrame(jdata['aaData'], columns=list(column_name))
    
    df['PER'] = df['PER'].apply(lambda s:pd.to_numeric(s, errors='coerce'))
    df['yield'] = df['yield'].apply(lambda s:pd.to_numeric(s, errors='coerce'))
    df['PBR'] = df['PBR'].apply(lambda s:pd.to_numeric(s, errors='coerce'))
    df['stock_id'] = df['stock_id'].astype(str)
    df['date'] = date_str
    
    df = df.drop(['dividend'],axis=1)

    df = df.fillna(0)
    df = df.filter(['stock_id','date','PER','yield','PBR'])
    df.insert(0,'group','tpex')
    
    return df.set_index(['stock_id','date'])

def crawl_twse_legalperson(date):
    # 上市公司法人買賣盤後資訊

    date_str = date.strftime('%Y-%m-%d')
    date_url = date.strftime('%Y%m%d')

    url = 'http://www.tse.com.tw/fund/T86?response=csv&date='+date_url+'&selectType=ALLBUT0999'

    # fake bowser
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    session = requests.Session()

    # get data     
    try:
        rdata = session.get(url, headers=headers)
        df = pd.read_csv(StringIO(rdata.text), header=1).dropna(how='all', axis=1).dropna(how='any')
    except:
        print('**WARN** no twse data at' + date_str )
        return None

    df = df.astype(str).apply(lambda s: s.str.replace(',',''))
    
    df['stock_id'] = df['證券代號'].str.replace('=','').str.replace('"','')
    df['date'] = date_str
    df = df.set_index(['stock_id', 'date'])

    df = df.apply(lambda s: pd.to_numeric(s, errors='coerce')).dropna(how='all', axis=1)
    df = df.apply(lambda s: s/1000)

    df['外資'] = df['外陸資買賣超股數(不含外資自營商)'] + df['外資自營商買賣超股數']
    df['投信'] = df['投信買賣超股數']
    df['自營商'] = df['自營商買賣超股數'] + df['自營商買賣超股數(避險)']
    df['三大法人'] = df['三大法人買賣超股數']

    df = df.filter(['外資','自營商','投信','三大法人'])
    df.insert(0,'group','twse')

    return df

def crawl_tpex_legalperson(date) :
    #上櫃三大法人買賣明細資訊
    
    date_str = date.strftime('%Y-%m-%d')

    # date format in url is like 108/01/01
    month_str = str(date.month) if date.month > 10 else '0' + str(date.month) 
    day_str = str(date.day) if date.day > 10 else '0' + str(date.day) 
    date_url = year_str = str(date.year-1911) + '/' + month_str + '/'+ day_str


    url = 'http://www.tpex.org.tw/web/stock/3insti/daily_trade/3itrade_hedge_result.php?l=zh-tw&o=csv&se=EW&t=D&d='+date_url+'&s=0,asc'

    #fake bowser
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    session = requests.Session()

    try:
        rdata = session.get(url, headers=headers)
        df = pd.read_csv(StringIO(rdata.text), header=1).dropna(how='all', axis=1).dropna(how='any')
    except:
        print('**WARN** no tpex data at' + date_str )
        return None

    df = df.astype(str).apply(lambda s: s.str.replace(',',''))

    df['stock_id'] = df['代號']
    df['date'] = date_str
    df = df.set_index(['stock_id', 'date'])

    df = df.apply(lambda s: pd.to_numeric(s, errors='coerce')).dropna(how='all', axis=1)
    df = df.apply(lambda s: s/1000)

    df = df.filter(['外資及陸資-買賣超股數','自營商-買賣超股數','投信-買賣超股數','三大法人買賣超股數合計'])
    df = df.rename(columns={'外資及陸資-買賣超股數':'外資','自營商-買賣超股數':'自營商','投信-買賣超股數':'投信','三大法人買賣超股數合計':'三大法人'})
    df.insert(0,'group','tpex')

    return df

def crawl_origin_twse_price(date):
    # 保留所有欄位
    date_report = date.strftime('%Y-%m-%d')

    # url上市公司日期格式(ex. 20190101)
    date_twse = date.strftime('%Y%m%d') 
    url_twse = 'http://www.tse.com.tw/exchangeReport/MI_INDEX?response=csv&date=' + date_twse + '&type=ALLBUT0999&_=1528951505187'
    
    #fake bowser
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    
    # crawl twse data       
    try:
        r_twse = requests.post(url_twse, headers)
    except:
        print('**WARN** requests cannot get twse data at', date_report)
        return None
    
    lines = r_twse.text.split('\n')
    newlines = []
    
    # 指數類欄位為6欄，個股數據欄位為17欄，
    # 過濾個股數據用「",」判斷符合加入newlines 中

    for line in lines:   
        if len(line.split('",')) == 17:
            newlines.append(line)
    
    s = '\n'.join(newlines)
    s = s.replace('=', '')

    if s == '':
        return None

    df = pd.read_csv(StringIO(s))
     
    df['漲跌價差'] = df['漲跌(+/-)'] + df['漲跌價差'].apply(lambda x: str(x))
    df['成交股數'] = df['成交股數'].apply(lambda x: str(x.replace(',','')))
    df['成交金額'] = df['成交金額'].apply(lambda x: str(x.replace(',','')))
    col_id = df['證券代號']
    col_name = df['證券名稱']
    df = df.apply(lambda s: pd.to_numeric(s, errors='coerce')).dropna(how='all', axis=1)
    df['stock_id'] = col_id
    df['name'] = col_name
    del df['證券代號']
    df['date'] = date_report

    return df.set_index(['stock_id', 'date'])

def crawl_origin_tpex_price(date): 
    # url上櫃公司日期格式(ex. 107/01/01)
    date_report = date.strftime('%Y-%m-%d')
    date_tpex = str(date.year-1911) + date.strftime('/%m/%d')

    url_tpex = 'http://www.tpex.org.tw/web/stock/aftertrading/daily_close_quotes/stk_quote_result.php?l=zh-tw&d=' + date_tpex + '&_=1531718901645'

    #fake bowser
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

    try:
        r_tpex = requests.post(url_tpex, headers)
        r_tpex.encoding = 'utf-8'

    except:
        print('**WARN** requests cannot get tpex data at', date_report)
        return None

    jdata = json.loads(r_tpex.text)
    df = pd.DataFrame()

    column_name = ["stock_id","name","收盤價","漲跌價差","開盤價","最高價","最低價","均價","成交股數","成交金額","成交筆數","最後買價","最後賣價","發行股數","次日參考價","次日漲停價","次日跌停價"]     

    df = pd.DataFrame(jdata['aaData'], columns=list(column_name))

    if len(df) == 0:
        return None

    df = df.applymap(lambda x: str(x.replace(',','')))
    col_id = df['stock_id']
    col_name = df['name']

    df = df.apply(lambda s: pd.to_numeric(s, errors='coerce')).dropna(how='all', axis=1)

    df['stock_id'] = col_id
    df['name'] = col_name
    df['date'] = date_report

    return df.set_index(['stock_id', 'date'])