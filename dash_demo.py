import dash
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
import plotly.plotly as py
from datetime import datetime

import sqlite3
import os

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)


conn = sqlite3.connect(os.path.join('data', "stock_data.db"))

def get_data(conn, table_name):
    
    sql = 'SELECT * FROM ' + table_name
    df = pd.read_sql(sql, conn, parse_dates=['date']).sort_values(by=['date'])

    return df

def get_data_by_intervel(conn, table, start_date, end_date):
    
    start = start_date.strftime('%Y-%m-%d')
    end = end_date.strftime('%Y-%m-%d')
    
    sql = 'SELECT * FROM ' + str(table) + ' WHERE date > "' + start +'"' + ' AND date < "' + end +'"' 
        
    df = pd.read_sql(sql, conn, parse_dates=['date']).sort_values(by=['date'])

    return df

def get_info(conn):

    sql = 'SELECT * FROM info'
    df = pd.read_sql(sql, conn)

    return df

def generate_table(dataframe, max_rows=10):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +
        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))],
        
        style={'margin':'0 auto'}
    )

# data init
start_date = datetime(2014,1,1)
end_date = datetime(2019,6,11)
# 個股資訊
info = get_info(conn)
# 本益比
per = get_data_by_intervel(conn, 'per', start_date, end_date)
# 收盤價
price = get_data_by_intervel(conn, 'price', datetime(2019,1,1), datetime(2019,6,11))
price['volume'] = price['volume'].apply(lambda x: x/1000)
# 月營收
revenue = get_data_by_intervel(conn, 'revenue', start_date, end_date)
# 財報
balance = get_data(conn,'balance')
income = get_data(conn,'income')
cashflow = get_data(conn,'cashflow')


app.layout = html.Div([
    html.Div([
        html.H4(children='個股資訊查詢'),
        dcc.Input(id='input-stockid', placeholder='Enter stock ID ...', value='2330', type='text'),
        html.Button('查詢', id='query-button', n_clicks=0),
    ], style={'width': '30%','text-align':'center','margin':'0 auto'}),
    
    # 個股近日資料
    html.Div([
        html.Div(id='basic-title')
    ], style={'width': '90%','text-align':'center','font-size':'18px','margin':'0 auto','margin-top':'20px','margin-bottom':'20px'}),
    html.Div([
        html.Div(id='basic-table')
    ], style={'width': '90%','text-align':'center','margin':'0 auto'}),

    # # 個股股價
    # html.Div([
    #     dcc.Graph(id='graph-candle'),
    # ], style={'height':'80%','width': '90%','text-align':'center','margin':'0 auto'}),

    # # 本益比箱型圖
    # html.Div([
    #     dcc.Graph(id='graph-box-per'),
    # ], style={'width': '45%','text-align':'center','margin':'0 auto', 'float':'left'}),

    # # 殖利率箱型圖
    # html.Div([
    #     dcc.Graph(id='graph-box-yield'),
    # ], style={'width': '45%','text-align':'center','margin':'0 auto', 'float':'left'}),
    
    # 月營收長條圖
    html.Div([
        dcc.Graph(id='graph-revenue-bar'),
    ], style={'width': '45%', 'display': 'inline-block', 'padding': '0 20','padding-left': '10px'}),

    # 月營收成長率
    html.Div([
        dcc.Graph(id='graph-revenue-gain'),
    ], style={'width': '45%', 'display': 'inline-block', 'padding': '0 20','padding-left': '10px'}),

    # EPS
    html.Div([
        dcc.Graph(id='graph-eps-bar'),
    ], style={'width': '45%', 'display': 'inline-block', 'padding': '0 20','padding-left': '10px'}),

    # 獲利能力
    html.Div([
        dcc.Graph(id='graph-income-rate'),
    ], style={'width': '45%', 'display': 'inline-block', 'padding': '0 20','padding-left': '10px'}),

])

@app.callback(
    dash.dependencies.Output('basic-title', 'children'),
    [dash.dependencies.Input('query-button','n_clicks')],
    [dash.dependencies.State('input-stockid', 'value')])
def update_basic_title(n_clicks, stock_id):
    return '{} 近期資訊'.format(stock_id)

@app.callback(
    dash.dependencies.Output('basic-table', 'children'),
    [dash.dependencies.Input('query-button','n_clicks')],
    [dash.dependencies.State('input-stockid', 'value')])
def update_basic_table(n_clicks, stock_id):
    # 資訊合併
    df1 = per[per.stock_id == stock_id].copy().tail(1)
    df1 = df1.filter(['stock_id','date','PBR','PER','yield'])
    
    df2 = info[info.stock_id == stock_id].copy()
    df2 = df2.filter(['stock_id','產業別','公司簡稱'])
    
    df3 = price[price.stock_id == stock_id].copy()
    df3 = df3.filter(['stock_id','date','close','volume'])
    
    df = pd.merge(df1,df2, on='stock_id')
    df = pd.merge(df,df3, on=['stock_id','date'])

    df = df.rename(columns={'stock_id': '代號','date':'日期','PBR':'股淨比','PER':'本益比','yield':'殖利率(%)','close':'收盤價','volume':'成交量'})
    df = df.filter(['代號','公司簡稱','產業別','日期','收盤價','成交量','殖利率(%)','本益比','股淨比'])
    return generate_table(df)

# 股價
# @app.callback(
#     dash.dependencies.Output('graph-candle', 'figure'),
#     [dash.dependencies.Input('query-button','n_clicks')],
#     [dash.dependencies.State('input-stockid', 'value')])
# def update_candle_figure(n_clicks, stock_id):
#     df = price[price.stock_id == stock_id]
#     traces =[]
#     traces.append(go.Candlestick(
#             x = df['date'],
#             open = df['open'],
#             high = df['high'],
#             low = df['low'],
#             close = df['close']
#     ))

#     return {
#         'data': traces,
#         'layout': go.Layout(
#             title='{} 股價走勢圖'.format(stock_id),
#             xaxis={'title': '日期',
#                    'range' : ['2019-03-10','2019-6-10']
#             },
#             yaxis={'title': '股價'},
#             hovermode='closest',
#             #legend={'x':0,'y':1.2}
#         )
#     }

# 本益比箱型圖
# @app.callback(
#     dash.dependencies.Output('graph-box-per', 'figure'),
#     [dash.dependencies.Input('query-button','n_clicks')],
#     [dash.dependencies.State('input-stockid', 'value')])
# def update_per_figure(n_clicks, stock_id):
#     df = per[per.stock_id == stock_id]
#     df['year'] = df['date'].apply(lambda x:x.strftime('%Y'))
#     traces =[]
#     traces.append(go.Box(
#         x = df['year'],
#         y = df['PER'],
#         boxpoints = 'outliers',
#         marker = dict(
#             color = 'rgb(107,174,214)'),
#         line = dict(
#             color = 'rgb(107,174,214)')
#     ))

#     return {
#         'data': traces,
#         'layout': go.Layout(
#             title='{} 本益比箱型圖'.format(stock_id),
#             xaxis={'title': '日期'},
#             yaxis={'title': '本益比'},
#             hovermode='closest',
#             )
#         }

# # 殖利率箱型圖
# @app.callback(
#     dash.dependencies.Output('graph-box-yield', 'figure'),
#     [dash.dependencies.Input('query-button','n_clicks')],
#     [dash.dependencies.State('input-stockid', 'value')])
# def update_per_figure(n_clicks, stock_id):
#     df = per[per.stock_id == stock_id ].copy()
#     df['year'] = df['date'].apply(lambda x:x.strftime('%Y'))
#     traces =[]
#     traces.append(go.Box(
#         x = df['year'],
#         y = df['yield'],
#         boxpoints = 'outliers',
#         marker = dict(
#             color = 'rgb(107,174,214)'),
#         line = dict(
#             color = 'rgb(107,174,214)')
#     ))

#     return {
#         'data': traces,
#         'layout': go.Layout(
#             title='{} 殖利率箱型圖'.format(stock_id),
#             xaxis={'title': '日期'},
#             yaxis={'title': '殖利率'},
#             hovermode='closest',
#             )
#         }    

# 月營收成長率
@app.callback(
    dash.dependencies.Output('graph-revenue-gain', 'figure'),
    [dash.dependencies.Input('query-button','n_clicks')],
    [dash.dependencies.State('input-stockid', 'value')])
def update_revenuegain_figure(n_clicks, stock_id):
    df = revenue[revenue.stock_id == stock_id]
    traces =[]

    traces.append(go.Scatter(
        x = df['date'],
        y = df['去年同月增減(%)'],
        mode = 'lines+markers',
        name = '單月'
    ))

    traces.append(go.Scatter(
        x = df['date'],
        y = df['去年同月增減(%)'].rolling(3,min_periods=1).mean(),
        mode = 'lines+markers',
        name = '近3月平均'
    ))

    traces.append(go.Scatter(
        x = df['date'],
        y = df['去年同月增減(%)'].rolling(12,min_periods=1).mean(),
        mode = 'lines+markers',
        name = '近12月平均'
    ))

    return {
        'data': traces,
        'layout': go.Layout(
            title='{} 月營收成長率走勢圖'.format(stock_id),
            xaxis={'title': '日期'},
            yaxis={'title': '月營收成長率(%)'},
            hovermode='closest',
            legend={'x':0,'y':1.2}
        )
    }

# 月營收長條圖
@app.callback(
    dash.dependencies.Output('graph-revenue-bar', 'figure'),
    [dash.dependencies.Input('query-button','n_clicks')],
    [dash.dependencies.State('input-stockid', 'value')]
    )
def update_revenuebar_figure(n_clicks, stock_id):
    df = revenue[revenue.stock_id == stock_id]
    traces =[]

    traces.append(go.Bar(
        x = df['date'],
        y = df['當月營收'],
        name = '月營收',
        marker = {'color':'orange'},
    ))

    return {
        'data': traces,
        'layout': go.Layout(
            title='{} 當月營收(仟元)'.format(stock_id),
            xaxis={'title': '日期'},
            yaxis={'title': '當月營收(仟元)'},
            hovermode='closest'
        )
    }

# EPS長條圖
@app.callback(
    dash.dependencies.Output('graph-eps-bar', 'figure'),
    [dash.dependencies.Input('query-button','n_clicks')],
    [dash.dependencies.State('input-stockid', 'value')]
    )
def update_epsbar_figure(n_clicks, stock_id):
    df = income[income.stock_id == stock_id]

    quarter_range = ['2014','','','','2015','','','','2016','','','','2017','','','','2018','','','','2019']
    traces =[]
    traces.append(go.Bar(
        x = list(range(0,20)),
        y = df['基本每股盈餘合計'],
        name = '每股盈餘',
        marker = {'color':'orange'},
    ))

    return {
        'data': traces,
        'layout': go.Layout(
            title='{} 每股盈餘(元)'.format(stock_id),
            xaxis={
                'title': '日期',
                'ticktext': quarter_range,
                'tickvals': list(range(0,20)),
            },  
            yaxis={'title': '每股盈餘(元)'},
            hovermode='closest'
        )
    }

# 獲利圖
@app.callback(
    dash.dependencies.Output('graph-income-rate', 'figure'),
    [dash.dependencies.Input('query-button','n_clicks')],
    [dash.dependencies.State('input-stockid', 'value')]
    )
def update_income_figure(n_clicks, stock_id):
    df = income[income.stock_id == stock_id]

    total_income = df['營業收入合計']
    net_income = df['本期淨利（淨損）']
    operating_income = df['營業利益（損失）']
    gross_income = df['營業毛利（毛損）']

    # 淨利率
    net_income_rate = round((net_income / total_income),4)*100
    # 營益率
    operating_income_rate = round((operating_income / total_income),4)*100
     # 毛利率
    gross_income_rate = round((gross_income / total_income),4)*100

    quarter_range = ['2014','','','','2015','','','','2016','','','','2017','','','','2018','','','','2019']
    traces =[]

    traces.append(go.Scatter(
        x = list(range(0,20)),
        y = net_income_rate,
        mode = 'lines+markers',
        name = '稅後淨利率'
    ))

    traces.append(go.Scatter(
        x = list(range(0,20)),
        y = operating_income_rate,
        mode = 'lines+markers',
        name = '營業利益率'
    ))

    traces.append(go.Scatter(
        x = list(range(0,20)),
        y = gross_income_rate,
        mode = 'lines+markers',
        name = '毛利率'
    ))
    
    return {
        'data': traces,
        'layout': go.Layout(
            title='{} 獲利能力圖'.format(stock_id),
            xaxis={
                'title': '日期',
                'ticktext': quarter_range,
                'tickvals': list(range(0,20)),
            },
            yaxis={'title': '獲利能力(%)'},
            hovermode='closest',
            legend={'x':0,'y':1.2}
        )
    }

if __name__ == '__main__':
    app.run_server(debug=True)