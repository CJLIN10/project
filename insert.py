import pandas as pd
import sqlite3

def insert_table(conn, df, table, method):
    
    table = str(table)
    
    if method == 'test':

        print('no insert data')

    elif method == 'merge':
         
        sql = 'SELECT * FROM '+ table
        
        df_old = pd.read_sql(sql, conn ,index_col=['stock_id', 'date'])
        df_new = pd.concat([df,df_old],axis=0)
        df_new = df_new.loc[~df_new.index.duplicated(keep='last')]
        df_new.to_sql(table, conn, if_exists='replace')
    
    elif method == 'replace':
        
        df.to_sql(table, conn, if_exists='replace')


def insert_taiex(conn, df, table, method):
    
    table = str(table)
    
    if method == 'test':

        print('no insert data')

    elif method == 'merge':
        
        df_old = pd.read_sql('SELECT * FROM taiex', conn ,index_col=['date'])
        df_new = df_old.append(df,sort=True).reset_index().drop_duplicates(subset=['date'], keep='last').set_index(['date'])
        df_new.to_sql('taiex',conn, if_exists='replace')

    elif method == 'replace':
        
        df.to_sql(table, conn, if_exists='replace')


def insert_info(conn, df):
    df.to_sql('info', conn, if_exists='replace')