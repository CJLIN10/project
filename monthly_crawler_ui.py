# 爬取月營收 GUI

style = {'description_width': 'initial'}

btn_date_start = widgets.DatePicker(
    disabled=False,
    value = datetime.today()
)

btn_date_end = widgets.DatePicker(
    disabled=False,
    value = datetime.today()
)

btn_crawl = widgets.Button(
    description='Crawl'
)

radio_method = widgets.RadioButtons(
    options={'test':'test', 'merge':'merge', 'replace':'replace' },
    value='test',
    description='Method:',
    disabled=False,
    style = style
)

radio_target = widgets.RadioButtons(
    options={'revenue': 'revenue' },
    value='revenue',
    description='Crawl target:',
    disabled=False,
    style = style
)


def crawl_target(b):
    
    start = btn_date_start.value
    end = btn_date_end.value
    target = radio_target.value
    method = radio_method.value
    
    print('start crawl target: ' + target +' method: ' + method)
    date_interval = list(rrule(MONTHLY, dtstart=start, until=end))
    df = pd.DataFrame()
    
    for date in date_interval :

        print(date.strftime('%Y-%m'))

        try:
            twse_revenue = crawl_twse_revenue(date)
            tpex_revenue = crawl_tpex_revenue(date)
            df = df.append(twse_revenue).append(tpex_revenue)
          
        except:
            print('no data at ' + date.strftime('%Y-%m'))
            continue
        
        time.sleep(5 + random.randint(0,5))
    
    insert_table(conn, df , target , method)    
    print('finish')    

btn_crawl.on_click(crawl_target) 
get_interval(conn,'revenue')

items = [radio_target, radio_method, btn_date_start, btn_date_end ,btn_crawl]
widgets.VBox(items)