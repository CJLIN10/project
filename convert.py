import requests
import pandas as pd
from io import StringIO
from datetime import datetime
import time
import sqlite3
import os
import random
import json
from dateutil.rrule import rrule, DAILY, MONTHLY 

def profit_interval(close):    
    # calculate profit in date interval

    profit =  close / close.shift(1)
    
    convert_profit = round(( profit.cumprod() - 1 )*100,2)
    
    return convert_profit[-1]

def convert_rev_date(date):
# convert date to revenue format 

    if date.month == 1:
        convert_date = str(date.year - 1) +'-12-01'
    elif date.month >= 10:
        convert_date = str(date.year) + '-' + str(date.month - 1)+'-01'
    else:
        convert_date = str(date.year) + '-0' + str(date.month - 1)+'-01'
    return convert_date

def calculate_bias(target): 
    # 計算乖離率
    ma5 = target.close.rolling(5,min_periods=1).mean().round(decimals=2)
    ma10 = target.close.rolling(10,min_periods=1).mean().round(decimals=2)
    ma20 = target.close.rolling(20,min_periods=1).mean().round(decimals=2)
    ma60 = target.close.rolling(60,min_periods=1).mean().round(decimals=2)
    ma120 = target.close.rolling(120,min_periods=1).mean().round(decimals=2)
    ma240 = target.close.rolling(240,min_periods=1).mean().round(decimals=2)
    
    return pd.DataFrame({
                'ma5': ma5, 
                'ma10': ma10,
                'ma20': ma20,
                'ma60': ma60,
                'ma120': ma120,
                'ma240': ma240,
                'bias5': ((target.close - ma5) / ma5 ).round(decimals=2),
                'bias10':((target.close - ma10) / ma10 ).round(decimals=2),
                'bias20':((target.close - ma20) / ma20  ).round(decimals=2),
                'bias60':((target.close - ma60) / ma60 ).round(decimals=2),
                'bias120':((target.close - ma120) / ma120 ).round(decimals=2),
                'bias240':((target.close - ma240) / ma240 ).round(decimals=2)
            })

def transfer_sheet(stock_id, dfs, target_sheet, year, season):
    if target_sheet == 'balance':
        trans = dfs[1].set_index(0).drop('會計項目', axis=0).apply(pd.to_numeric).dropna().T
        by_season = trans.loc[:1]
    
    elif target_sheet == 'income':
        trans = dfs[2].set_index(0).drop('會計項目', axis=0).apply(pd.to_numeric).dropna().T
        by_season = trans.loc[:1] if (season == 1 or season == 4 ) else trans.loc[3:3]
        
    elif target_sheet == 'cashflow':
        trans = dfs[3].set_index(0).drop('會計項目', axis=0).apply(pd.to_numeric).dropna().T
        by_season = trans.loc[:1]
        
    if season == 1:
        report_date = str(year) + '-05-15'
    elif season == 2:
        report_date = str(year) + '-08-14'
    elif season == 3:
        report_date = str(year) + '-11-14'
    elif season == 4:
        report_date = str(year + 1) + '-03-31'
        
    by_season.insert(0, 'date', report_date)
    by_season.insert(1, 'stock_id', stock_id)
    by_season.insert(2, '年度', str(year))
    by_season.insert(3, '季度', str(season))
    
    df = by_season.loc[:,~by_season.columns.duplicated()]

    df = df.set_index(['stock_id','date'])
       
    return df

def clean_to_single(cumulate, year, season):  
    # 轉換年度資料為單季

    by_year = cumulate[cumulate['年度'] == year]
    
    first = by_year[by_year['季度'] == '1'].reset_index().set_index('stock_id')
    second = by_year[by_year['季度'] == '2'].reset_index().set_index('stock_id')
    third = by_year[by_year['季度'] == '3'].reset_index().set_index('stock_id')  
    fouth = by_year[by_year['季度'] == '4'].reset_index().set_index('stock_id') 
    
    if season == '1':
        by_season = by_year[by_year.季度 == '1']
        return by_season

    elif season == '2':
        date = second['date']  
        first = first.drop(['date', '年度','季度'], axis=1)
        second = second.drop(['date', '年度','季度'], axis=1)
        diff = second - first 
   
    elif season == '3':
        date = third['date']  
        second = second.drop(['date', '年度','季度'], axis=1)
        third = third.drop(['date', '年度','季度'], axis=1)
        diff = third - second 
       
    elif season == '4':    
        date = fouth['date']  
        third = third.drop(['date', '年度','季度'], axis=1)
        fouth = fouth.drop(['date', '年度','季度'], axis=1)
        diff = fouth - third 
     
    diff['date'] = date
    diff['年度'] = year
    diff['季度'] = season

    return diff.reset_index().set_index(['stock_id','date'])
       