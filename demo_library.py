import requests
import pandas as pd
from io import StringIO
from datetime import datetime
import time
import sqlite3
import os
import random

import matplotlib.pyplot as plt
import numpy as np
%matplotlib inline
import ipywidgets as widgets
from dateutil.rrule import rrule, DAILY, MONTHLY 

import json 

from crawler import (
    crawl_taiex,
    crawl_info,
    crawl_twse_price,
    crawl_tpex_price,
    crawl_twse_revenue,
    crawl_tpex_revenue,
    crawl_twse_per,
    crawl_tpex_per,
    crawl_twse_legalperson,
    crawl_tpex_legalperson,
)
from insert import (
    insert_taiex, 
    insert_table,
    insert_info
)
from get_data import (
    get_info,
    get_taiex,
    get_interval,
    get_data,
)