# 財報檔案轉換 html to dataframe

import pandas as pd
import time
import datetime
import os
import sqlite3
import numpy as np
import ipywidgets as widgets

from io import StringIO

from get_data import (
    get_info,
    get_statement_inderval,
)
from convert import (
    transfer_sheet
)
from insert import (
    insert_table
)

# init data
conn = sqlite3.connect(os.path.join('data', "stock_data.db"))
stock_list = get_info(conn).index.values
get_statement_inderval(conn,'balance')

# ui
style = {'description_width': 'initial'}
btn_convert = widgets.Button(
    description='Convert'
)
input_year = widgets.BoundedIntText(
    value=2018,
    min=1900,
    max=2200,
    step=1,
    description='year:',
    disabled=False
)
radio_season = widgets.RadioButtons(
    options={'1': 1, '2': 2, '3': 3, '4': 4 },
    value=1,
    description='season:',
    disabled=False,
    style = style
)
radio_method = widgets.RadioButtons(
    options={'test': 'test', 'merge':'merge', 'replace':'replace' },
    value='test',
    description='Method:',
    disabled=False,
    style = style
)

def convert_statment(b):
    print('start')
    year =input_year.value
    season = radio_season.value
    
    balance = pd.DataFrame()
    income_cum = pd.DataFrame()
    cashflow_cum = pd.DataFrame()

    method = radio_method.value

    for stock_id in stock_list:

        folder = os.path.join('data', 'financial_statement', str(year) + str(season))    
        path = os.path.join(folder , stock_id + '.html')

        # 不為 html 結尾 或檔案太小略過
        try:
            path[-4:] != 'html' or os.stat(path).st_size < 10000
            f = open(path, 'r', encoding='utf-8')
            content = f.read()

            dfs = pd.read_html(content)
            balance_by_season = transfer_sheet(stock_id, dfs, 'balance', year, season)
            income_by_season = transfer_sheet(stock_id, dfs, 'income', year, season)
            cashflow_by_season = transfer_sheet(stock_id, dfs, 'cashflow', year, season)

            balance = balance.append(balance_by_season)
            income_cum = income_cum.append(income_by_season)
            cashflow_cum = cashflow_cum.append(cashflow_by_season)
            
        except:
            print('open html failed',stock_id)
        
    insert_table(conn, balance, 'balance_rdata', method)
    print('finish {} {},{} data to balance_rdata'.format(method, year, season))
    insert_table(conn, income_cum, 'income_rdata', method)
    print('finish {} {},{} data to income_rdata'.format(method, year, season))
    insert_table(conn, cashflow_cum, 'cashflow_rdata', method)
    print('finish {} {},{} data to cashflow_rdata'.format(method, year, season))
    
btn_convert.on_click(convert_statment) 

items = [input_year, radio_season, radio_method ,btn_convert]
widgets.VBox(items)