# 財報檔案清理 
# 1. 股東權益欄位統一
# 2. 累計轉為單季

import pandas as pd
import time
import datetime
import os
import sqlite3
import numpy as np
import ipywidgets as widgets

from get_data import (
    get_info,
    get_statement_inderval
)
from convert import (
    transfer_sheet,
    clean_to_single
)
from insert import (
    insert_table
)

conn = sqlite3.connect(os.path.join('data', "stock_data.db"))

style = {'description_width': 'initial'}

btn_clean_balance = widgets.Button(
    description='Convert balance'
)
btn_clean_cum = widgets.Button(
    description='Convert cum to single'
)

input_year = widgets.BoundedIntText(
    value=2018,
    min=1900,
    max=2200,
    step=1,
    description='year:',
    disabled=False
)

radio_season = widgets.RadioButtons(
    options={'1': 1, '2': 2, '3': 3, '4': 4 },
    value=1,
    description='season:',
    disabled=False,
    style = style
)

radio_method = widgets.RadioButtons(
    options={'test': 'test', 'merge':'merge', 'replace':'replace' },
    value='test',
    description='Method:',
    disabled=False,
    style = style
)

# get raw data
balance_rdata = pd.read_sql('SELECT * FROM balance_rdata',conn , index_col=['stock_id', 'date'])
income_rdata = pd.read_sql('SELECT * FROM income_rdata',conn , index_col=['stock_id', 'date'])
cashflow_rdata = pd.read_sql('SELECT * FROM cashflow_rdata',conn , index_col=['stock_id', 'date'])

# print statement_inderval
get_statement_inderval(conn,'balance')
get_statement_inderval(conn,'income')
get_statement_inderval(conn,'cashflow')

def clean_balance_rdata(b):
    # 資產負債表轉換&存入db

    balance_rdata.get('權益總計').fillna(0, inplace=True)
    balance_rdata.get('權益總額').fillna(0, inplace=True)

    balance_rdata['股東權益'] = balance_rdata['權益總計'] + balance_rdata['權益總額']

    insert_table(conn, balance_rdata, 'balance', method='replace')
    print('set balance finish')
    
def clean_cum(b):    
    
    # 累計轉換單季資料&存入db
    year = str(input_year.value)
    season = str(radio_season.value)
    method = str(radio_method.value)

    # 綜合損益表累計轉換為單季
    income_by_single = clean_to_single(income_rdata, year, season)
    insert_table(conn, income_by_single, 'income', method)
    print('{} {},{} income finish'.format(method, year, season))

    # 現金流量表累計轉換為單季
    cashflow_by_single = clean_to_single(cashflow_rdata, year, season)
    insert_table(conn, cashflow_by_single, 'cashflow', method)
    print('{} {},{} cashflow finish'.format(method, year, season))

    
btn_clean_balance.on_click(clean_balance_rdata)
btn_clean_cum.on_click(clean_cum) 

items = [input_year, radio_season, radio_method , btn_clean_balance, btn_clean_cum]
widgets.VBox(items)