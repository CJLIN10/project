    
class convertDate:
    def __init__(self, year, month, day):
        self.year = year
        self.month = month 
        self.day = day
    
    def toDaily(self):
        if int(self.month) < 1  or int(self.month) > 12 :
            return 'dateformat error'
        
        elif int(self.day) < 1  or int(self.day) > 31 :
            return 'dateformat error'
        
        else:
            year_str = str(self.year)
            month_str = str(self.month) if int(self.month) >= 10 else '0' + str(self.month)
            day_str = str(self.day) if int(self.day) >= 10 else '0' + str(self.day)

            return  year_str + '-'+ month_str +'-'+ day_str
    
    def toRevenue(self):
        if int(self.month) < 1  or int(self.month) > 12 :
            return 'dateformat error'
        
        elif int(self.day) < 1  or int(self.day) > 31 :
            return 'dateformat error'
        
        elif self.month == 1 :
            year_str, month_str, day_str = (str(int(self.year) - 1) , '11', '01') if int(self.day) < 10 else (str(int(self.year) - 1) , '12' , '01')
        
        elif self.month == 2 :
            year_str, month_str, day_str = (str(int(self.year) - 1) , '12' , '01') if int(self.day)  < 10 else (str(self.year) , '01' , '01')

        elif self.month == 10 :  
            year_str, month_str, day_str = (str(self.year), '08' , '01') if int(self.day) < 10 else (str(self.year) , '09' , '01')
    
        elif self.month == 11 :  
            year_str, month_str, day_str = (str(self.year), '09' , '01') if int(self.day) < 10 else (str(self.year) , '10' , '01')

        elif self.month == 12 :
            year_str, month_str, day_str = (str(self.year), '10' , '01') if int(self.day) < 10 else (str(self.year) , '11', '01')
    
        else :
            year_str, month_str, day_str = (str(self.year), '0'+ str(int(self.month)-2) , '01') if int(self.day) < 10 else ( str(self.year) , '0'+str(int(self.month-1)) , '01')
        
        return  year_str + '-'+ month_str +'-'+ day_str
    
    def toStatement(self):
        date_condition = int(str(self.month)+ str(self.day))
        
        if int(self.month) < 1  or int(self.month) > 12 :
            return 'dateformat error'
        
        elif int(self.day) < 1  or int(self.day) > 31 :
            return 'dateformat error'
        
        elif (date_condition < 331): 
            return str(int(self.year)-1) + '-11-14' # 去年度第三季

        elif (date_condition >= 331) and (date_condition < 515):
            return str(self.year) + '-03-31' # 去年度第四季
        
        elif (date_condition >= 515) and (date_condition < 814):
            return str(self.year) + '-05-15' # 當年度第一季
       
        elif (date_condition >= 814) and (date_condition < 1114):
            return str(self.year) + '-08-14' # 當年度第二季
        
        elif (date_condition >= 1114) :
            return str(self.year) + '-11-14' # 當年度第三季
        
        
